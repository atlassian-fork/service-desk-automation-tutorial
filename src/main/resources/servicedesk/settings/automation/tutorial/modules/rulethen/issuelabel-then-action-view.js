define("servicedesk/settings/automation/tutorial/modules/rulethen/issue-label-then-action-view", [
    "jquery",
    "automation/underscore",
    "automation/backbone-brace",
    "servicedesk/internal/agent/settings/automation/util/form-mixin/form-mixin"
], function (
    $,
    _,
    Brace,
    FormMixin
) {
    return Brace.View.extend({
        template: ServiceDesk.Templates.Agent.Settings.Automation.Tutorial.Modules.RuleThen.drawIssueLabelForm,
        mixins: [FormMixin],

        dispose: function() {
            this.undelegateEvents();
            this.stopListening();
        },

        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });
});